package oop.asg;

public class BigInteger
{
    private String value;
    public BigInteger(long init){
        value = init + "";
    }

    public BigInteger(String init) {
        if(init.length()==0) value=init;
        else{
            if(init.charAt(0)!='-') {
                int i = 0;
                String str = init;
                while (init.charAt(i) == '0' && i < init.length() - 1) {
                    str = init.substring(i + 1);
                    i++;
                }
                value = str;
            }
            else{
                int i = 0;
                String str = init.substring(1);
                while(init.charAt(i+1)== '0'&& i < init.length()-1-1){
                    str = init.substring(i+2);
                    i++;
                }
                value ="-"+str;
            }
        }
    }

    public String toString() {
        return value;
    }

    public boolean equals(Object other) {
        return toString().equals(other.toString());
    }

    public long toLong() {
        return Long.parseLong(value);
    }

    public void plus(int n1[], int n2[], int max, int sum[]){
        int carry = 0;
        for(int i=0; i<max; i++){
            sum[i]=(n1[i]+n2[i]+carry)%10;
            if((n1[i]+n2[i]+carry)>=10){
                carry=1;
            }
            else{
                carry=0;
            }
        }
        sum[max]=carry;
    }

    public void minus(int n1[], int n2[], int max, int dif[]){
        int carry=0;
        for(int i=0; i<max; i++){
            if((n1[i]-n2[i]-carry)<0){
                dif[i]=n1[i]+10-n2[i]-carry;
                carry=1;
            }
            else{
                dif[i]=n1[i]-n2[i]-carry;
                carry=0;
            }
        }
    }

    public BigInteger add(BigInteger other) {
        BigInteger adding = new BigInteger("");
        if((this.value.charAt(0)!='-')&&(other.value.charAt(0)!='-')) {
            int max = (value.length() < other.value.length()) ? other.value.length() : value.length();
            int sum[] = new int[max+1];

            int n1[] = new int[max];
            int n2[] = new int[max];

            for (int i = 0; i < value.length(); i++) {
                n1[i] = Character.getNumericValue(value.charAt(value.length() - 1 - i));
            }
            for (int i = 0; i < other.value.length(); i++) {
                n2[i] = Character.getNumericValue(other.value.charAt(other.value.length() - 1 - i));
            }
            plus(n1, n2, max, sum);
            for (int i = max - 1 + sum[max]; i >= 0; i--) {
                adding.value = adding.value + Integer.toString(sum[i]);
            }
        }
        else if((this.value.charAt(0)=='-')&&(other.value.charAt(0)=='-')){
            int max = (value.length() < other.value.length()) ? other.value.length()-1 : value.length()-1;
            int sum[] = new int[max+1];

            int n1[] = new int[max];
            int n2[] = new int[max];

            for(int i=0; i<value.length()-1; i++){
                n1[i] = Character.getNumericValue(value.charAt(value.length()-i-1-1));
            }
            for(int i=0; i<other.value.length()-1; i++){
                n2[i] =  Character.getNumericValue(other.value.charAt(other.value.length()-i-1-1));
            }

            plus(n1, n2, max, sum);
            for(int i = max-1+sum[max]; i>=0; i--){
                adding.value = adding.value + Integer.toString(sum[i]);
            }
            adding.value="-"+adding.value;
        }
        else if((this.value.charAt(0)!='-')&&(other.value.charAt(0)=='-')){
            int max = (value.length() < other.value.length()-1) ? other.value.length()-1 : value.length();
            int sum[] = new int[max];

            int n1[] = new int[max];
            int n2[] = new int[max];

            for(int i=0; i<value.length(); i++){
                n1[i] =  Character.getNumericValue(value.charAt(value.length()-i-1));
            }
            for(int i=0; i<other.value.length()-1; i++){
                n2[i] = Character.getNumericValue(other.value.length()-i-1-1);
            }

            BigInteger b3 = new BigInteger(other.value.substring(1));

            if(this.compareTo(b3)==1){
                minus(n1, n2, max, sum);
                for(int i=max-1; i>=0; i--){
                    adding.value=adding.value+Integer.toString(sum[i]);
                }
            }
            else{
                minus(n2, n1, max, sum);
                for(int i=max-1; i>=0; i--){
                    adding.value=adding.value+Integer.toString(sum[i]);
                }
                adding.value="-"+adding.value;
            }
        }
        else{
            int max = value.length()-1 < other.value.length() ? other.value.length() : value.length()-1;
            int sum[] = new int[max+1];

            int n1[] = new int[max];
            int n2[] = new int[max];

            for(int i=0; i<value.length()-1; i++){
                n1[i] = Character.getNumericValue(value.charAt(value.length()-i-1-1));
            }
            for(int i=0; i<other.value.length(); i++){
                n2[i] = Character.getNumericValue(other.value.charAt(other.value.length()-i-1));
            }

            BigInteger b3 = new BigInteger(value.substring(1));
            if(b3.compareTo(other)==1){
                minus(n1, n2, max, sum);
                for(int i=max-1; i>=0; i--){
                    adding.value=adding.value+Integer.toString(sum[i]);
                }
                adding.value="-"+adding.value;
            }
            else{
                minus(n2, n1, max, sum);
                for(int i=max-1; i>=0; i--){
                    adding.value=adding.value+Integer.toString(sum[i]);
                }
            }
        }

        return adding;
    }

    public BigInteger subtract(BigInteger other) {
        BigInteger subt = new BigInteger("");

        int max = value.length()<other.value.length() ? other.value.length():value.length();
        int dif[] = new int[max];

        int n1[] = new int[max];
        int n2[] = new int[max];

        for(int i=0; i<value.length(); i++){
            n1[i]=Character.getNumericValue(value.charAt(value.length()-i-1));
        }
        for(int i=0; i<other.value.length(); i++){
            n2[i]=Character.getNumericValue(other.value.charAt(other.value.length()-i-1));
        }

        minus(n1, n2, max, dif);

        for(int i=max-1; i>=0; i--){
            subt.value=subt.value+Integer.toString(dif[i]);
        }
        return subt;
    }
    public int compareTo(BigInteger other){
        int l1 = value.length();
        int l2 = other.value.length();
        int check = 0;

        if((this.value.charAt(0)!='-')&&(other.value.charAt(0)!='-')){
            if(l1>l2) check=1;
            else if(l1<l2) check=-1;
            else{
                int num1[] = new int[l1];
                int num2[] = new int[l1];
                for(int i=0; i<l1; i++){
                    num1[i]=Character.getNumericValue(value.charAt(i));
                    num2[i]=Character.getNumericValue(other.value.charAt(i));
                }
                int i=0;
                while(i<l1){
                    if(num1[i]>num2[i]){
                        check=1;
                        break;
                    }
                    else if(num1[i]==num2[i]) i++;
                    else{
                        check=-1;
                        break;
                    }
                }
                if(i==l1) check=0;

            }
        }
        else if((value.charAt(0)!='-')&&(other.value.charAt(0)=='-')) check=1;
        else if((value.charAt(0)=='-')&&(other.value.charAt(0)!='-')) check=-1;
        else if((value.charAt(0)=='-')&&(other.value.charAt(0)=='-')){
            if(l1>l2) check=-1;
            else if(l1<l2) check=1;
            else{
                String str1 = value.substring(1);
                String str2 = other.value.substring(1);
                int num1[] = new int[str1.length()];
                int num2[] = new int[str2.length()];
                for(int i=0; i<str1.length(); i++){
                    num1[i]=Character.getNumericValue(str1.charAt(i));
                    num2[i]=Character.getNumericValue(str2.charAt(i));
                }
                int k=0;
                for(int i =0;i<str1.length(); i++){
                    if(num1[i]>num2[i]){
                        check=-1;
                        break;
                    }
                    else if(num1[i]==num2[i]) k=k+1;
                    else{
                        check=1;
                        break;
                    }
                }
                if(k==str1.length()){
                    check=0;
                }
            }
        }
        return (check);
    }
    public BigInteger clone() {
        BigInteger bi_clone = new BigInteger(value);
        return bi_clone;
    }
}



